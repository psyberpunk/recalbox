# Release notes

## Version 6.0-rc3

## News
- Raspberry Pi3b+ & CM3 compatibility
- Updated emulator names! catsfc -> snes9x2005, pocketsnes -> snes9x2002, snes9x_next -> snes9x2010, pce -> mednafen_pce_fast, vb -> mednafen_vb, imame -> mame2000, mame078 -> mame2003, fba -> fbalpha
- New emulator! Pokemon Mini on all systems (RetroArch core: libretro-pokemini)
- New emulator! Mattel Intellivision on all systems (RetroArch core: libretro-freeintv)
- New emulator! Fairchild Channel-F on all systems (RetroArch core: libretro-freechanf)
- New emulator! Atari Jaguar on x86/64 (RetroArch core: libretro-virtualjaguar)
- New emulator! MGT Sam Coupé on all systems (Standalone simcoupe)
- New emulator! Tangerine Oric/Atmos and later clones on all raspberry (Standalone oricutron)
- New emulator! Atari 8bits series on all systems (800, XL, XE) and Atari 5200 console (RetroArch core: libretro-atari800) - Thanks to Dubbows!
- New emulator! PC-FX emulator on x86 and XU4 systems (Retroarch core: libretro-beetle-pcfx)
- New emulator! PC-98xx series on all systems but odroidC2 (Retroarch core: libretro-np2kai)
- New System! SNES extensions Satellaview and SuFami Turbo added as own systems
- New System! NeoGeo CD with audio track support (via RetroArch core libretro-fba)
- New System! Amiga CD32 with audio track support (via Amiberry)
- New Core! Added mame2003-plus libretro core. A MAME078 version with added games support plus many fixes and improvements. DAT files available in rom folder
- New free game! "Mission: Liftoff" for Thomson emulator. 
- New free game! "Flower" for Amiga emulator. 
- New Capcom RB intro video
- New PCEngine intro video
- Add support for Mayflash GameCube adapter
- Add 7z file support for many libretro cores
- Add overlay possibility for advmame core
- Add new Screensaver "DEMO" to play continuously random games (Press START/ENTER to play the current game)

### Improvements
- KODI updated to 17.6
- Youtube plugin for KODI updated to 6.3.1 
- Joypads management updated, mostly impacting joypads advertising as a complete keyboard.
- Preconfigured gamepads file rewritten
- x86: Now requires a CPU that can handle at least SSE and SS2 (Core2duo and higher)
- x86: Add support for newer AMD GPU
- Retroarch updated to version 1.7.6!
- Retroarch cheats updated to version 1.7.6!
- Emulator updated! 4DO updated to lastest revision. Also available on Rpi3 (3B+ recommended).
- Emulator updated! Amiberry (Amiga) - Huge up! Add supports of IPF, RP9, CD, zip, 7z & Retroarch pad configuration - Available also on Odroid XU4.
- Emulator updated! DoxBox updated to December 2018 version.
- Emulator updated! ScummVM and ResidualVM updated to January 2019 versions.
- Libretro core updated! 81 (EX-81) updated to latest version.
- Libretro core updated! Atari800 (Atari 5200Atari 8bits) updated to latest version.
- Libretro core updated! Beetle Lynx (Atari Lynx) updated to latest version. Support Headerless roms.
- Libretro core updated! Beetle NGP (Neo Geo Pocket/Color) updated to latest version.
- Libretro core updated! Beetle PCE (PCEngine-CD) updated to latest version.
- Libretro core updated! Beetle PSX (Playstation 1) updated to latest version.
- Libretro core updated! Beetle PSX-HW (Playstation 1) updated to latest version.
- Libretro core updated! Beetle SGX (PCE/PCE-CD/SGX) updated to latest version.
- Libretro core updated! Beetle VB (Virtual Boy) updated to latest version.
- Libretro core updated! Beetle Wonderswan (Wonderswan/Color) updated to latest version.
- Libretro core updated! BlueMSX (MSX) updated to latest version.
- Libretro core updated! Caprice32 (Amstrad) updated to latest version.
- Libretro core updated! Crocods (Amstrad) updated to latest version.
- Libretro core updated! DeSmuME (NDS) updated to latest version.
- Libretro core updated! FBA Libretro core updated to latest version. Recommended Romset: 0.2.97.44
- Libretro core updated! FCEUmm (Nintendo NES) updated to latest version.
- Libretro core updated! fMSX (MSX) updated to latest version.
- Libretro core updated! FreeIntV (Intellivision) updated to latest version.
- Libretro core updated! Fuse (ZX Spectrum) updated to latest version.
- Libretro core updated! Gambatte (Nintendo GB/Color) updated to latest version.
- Libretro core updated! Genesis Plus GX (Sega MD/GG/MS/CD) updated to latest version.
- Libretro core updated! GNupeN64 (Nintendo64) updated to latest version.
- Libretro core updated! GPSP (Nintendo GBA) updated to latest version.
- Libretro core updated! GW (Game & Watch) updated to latest version.
- Libretro core updated! Handy (Atari Lynx) updated to latest version.
- Libretro core updated! Hatari (Atari ST) updated to latest version.
- Libretro core updated! iMAME (Mame 2000) updated to latest version.
- Libretro core updated! Lutro (Lua Game Engine) updated to latest version.
- Libretro core updated! MAME2003 (Mame 2003) updated to latest version.
- Libretro core updated! MAME2010 (Mame 2010) updated to latest version.
- Libretro core updated! MelonDS (NDS) updated to latest version.
- Libretro core updated! mGBA (Nintendo GBA) updated to latest version.
- Libretro core updated! Nestopia (Nintendo NES) updated to latest version.
- Libretro core updated! NxEngine (Cave Story) updated to latest version.
- Libretro core updated! O2EM (Odyssey²) updated to latest version.
- Libretro core updated! PCSX (Playstation 1) updated to latest version.
- Libretro core updated! Picodrive (Sega MS/MD/CD/32X) updated to latest version.
- Libretro core updated! PocketSnes (Nintendo SNES) updated to latest version.
- Libretro core updated! PrBoom (Doom engine) updated to latest version.
- Libretro core updated! ProSystem (Atari 7800) updated to latest version.
- Libretro core updated! Px68k (Sharp X68000) updated to latest version.
- Libretro core updated! QuickNES (Nintendo NES) updated to latest version.
- Libretro core updated! Snes9x (Nintendo SNES) updated to latest version.
- Libretro core updated! Snes9x2005 (Nintendo SNES, formerly CatSFC) updated to latest version.
- Libretro core updated! Snes9x2010 (Nintendo SNES, formerly Snes9x-Next) updated to latest version.
- Libretro core updated! Stella (Atari 2600) updated to latest version.
- Libretro core updated! TGBDual (Nintendo GB/Color) updated to latest version.
- Libretro core updated! Theodore (Thomson) - Supports now TO8, TO8D, TO9, TO9+ & MO5 machines
- Libretro core updated! VecX (Vectrex) updated to latest version.
- Libretro core updated! Vice (Commodores 8bits) updated to latest version.
- GPIO arcade driver upgraded: now support I2C and GPIO reconfiguring from the command line
- EmulationStation: Now automatically reboots once the upgrade is ready
- "overlays" folder and roms subfolders automatically created
- Updated all MAME hiscore/cheat files in bios folder - Thanks to olivierdroid92!
- Static IP configuration available for WIFI connections (in recalbox.conf)
- Add support for AZERTY/QWERTY virtual keyboards
- ScummVM/ResidualVM now use Recalbox pad configurations

### Fixes
- bluetooth detection on Odroid XU4
- bluetooth recovery on Odroid XU4
- EmulationStation: better cyrillic display
- EmulationStation: fix scraped folders display
- MSX Emulator: Fixed Bluemsx core
- PS3/PS4 pads are working now
- SSH is now working when moving the share on Fat32/exFat partitions
- Recalbox sends 0000 to BT devices asking for pincode
- WIFI settings saved in a more secure way

---

## Version 18.07.13 - 2018-07-13

### Improvements
- Bump desmume to desmume2015
- EmulationStation: Arcade roms fullname in Netplay GUID
- EmulationStation: No game launch if core doesn't match

### Fixes
- SELECT as hotkey sometimes messy with arcade
- EmulationStation: remove netplay popup to prevent some crash
- Wifi: options were not saved with nfs cifs or configurations
- N64 rice & GlideN64: fix blank screen
